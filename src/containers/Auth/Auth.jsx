import React, { Component } from 'react';
import ReactGA from 'react-ga';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {withRouter, Redirect} from 'react-router-dom';
import {bindActionCreators} from 'redux';
//import ButtonBase from 'material-ui/ButtonBase';
import LinearProgress from 'material-ui/Progress/LinearProgress';

import Fade from 'material-ui/transitions/Fade';

import * as authActions from '../../redux-modules/auth/actions';
import LoginForm from './Form/Login/Login';
import * as routes from '../../routes/Routes';

//import googleLogo from '../../assets/img/google.png';
import sisamexLogoSinTexto from '../../assets/img/sisamex_logo_sin_texto.png';

import classes from './Auth.scss';

/*const styles = {
    Google: {
        borderRadius: '5px',
        boxShadow: '2px 2px 5px #7f8fa2',
        padding: '10px',
        width: '100%'
    }
};*/

class Auth extends Component {

    componentDidMount = () => {
        ReactGA.pageview(window.location.pathname + window.location.search);
        return this.props.isAuthenticated ? this.props.history.push(routes.HOME) : null
    };

    submitHandler = (values) => {
        return this.props.onAuth(values.user, values.password);
    };

    render() {
        let linearProgress = <div className={classes.AuthLoader} />;
        if(this.props.loading) {
            linearProgress = <Fade in><LinearProgress color="primary"/></Fade>;
        }

        let authRedirect = null;
        if(this.props.isAuthenticated) {
            authRedirect = <Redirect to={routes.HOME} />;
        }

        return (
            <Fade in timeout={500}>
                <div className={classes.Auth}>
                {authRedirect}
                <div className={classes.AuthContainer}>
                    {linearProgress}
                    <div className={classes.AuthSubContainer}>
                        <div className={classes.LogoContainer}>
                            <img src={sisamexLogoSinTexto} alt="SISAMEX"/>
                        </div>

                        <h1 className={classes.Title}>Monitoreo de medios</h1>

                        <div className={classes.AuthFormContainer}>

                            <LoginForm onSubmit={this.submitHandler} error={this.props.error}/>

                            {/*<h2 className={classes.TitleSocial}><span>ó</span></h2>
                            <div className={classes.GoogleLogoContainer}>
                                <ButtonBase
                                    focusRipple
                                    style={styles.Google}
                                >
                                    <img src={googleLogo} alt="Google"/>
                                    <span>Inicia sesión con Google</span>
                                </ButtonBase>
                            </div>*/}
                        </div>
                    </div>

                </div>
            </div>
            </Fade>
        );
    }
}

const mapStateToProps = state => {
    return {
        loading: state.auth.loading,
        isAuthenticated: state.auth.token !== null
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAuth: bindActionCreators(authActions.auth, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Auth));

Auth.propTypes = {
  error: PropTypes.string,
  history: PropTypes.object,
  isAuthenticated: PropTypes.bool.isRequired,
  loading: PropTypes.bool.isRequired,
  onAuth: PropTypes.func.isRequired
};