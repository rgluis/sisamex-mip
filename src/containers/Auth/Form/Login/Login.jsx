import React from 'react';
import { reduxForm, Field } from 'redux-form';

import TextField from '../../../../components/UI/TextField/TextField';
import Button from '../../../../components/UI/Button/Button';
import {Typography} from 'material-ui';

import classes from './Login.scss';

const validate = (values) => {
    const errors = {};

    if(!values.user) {
        errors.user = 'Campo requerido';
    }

    if(!values.password) {
        errors.password = 'Campo requerido';
    }

    return errors;
};

const renderTextField = ({
         input,
         label,
         meta,
         ...custom
     }) => {
    const errorBool = meta.touched && meta.error ? true : false;
    return (
        <TextField
            {...input}
            margin="normal"
            fullWidth
            type={custom.type}
            label={label}
            error={errorBool || meta.submitFailed}
            helperText={errorBool ? meta.error : null}
        />
    );
};

let LoginForm = ({handleSubmit, pristine, reset, submitting, error}) => (
    <form onSubmit={handleSubmit} className={classes.Form}>
        <Field name="user" type="text" label="Usuario" component={renderTextField}/>
        <Field name="password" type="password" label="Contraseña" component={renderTextField}/>

        <Button
            type="submit"
            variant="raised"
            color="primary"
            disabled={pristine || submitting}
            fullWidth>Iniciar Sesión</Button>
        {error && <Typography
            color="error"
            style={{marginTop: '16px'}}
        >{error}</Typography>}
    </form>
);

LoginForm = reduxForm({
    form: 'login',
    validate
})(LoginForm);

export default LoginForm;