import React from 'react';
import classes from './User.scss';

import IconButton from 'material-ui/IconButton';
import LogoutIcon from 'material-ui-icons/PowerSettingsNew';

const user = (props) => (
    <div style={props.style} className={classes.User}>
        <div>
            <p>{props.name}</p>
            <p>{props.email}</p>
        </div>
        <div>
            <IconButton arial-label="Cerrar Sesión" onClick={props.logoutClicked}>
                <LogoutIcon />
            </IconButton>
        </div>
    </div>
);

export default user;