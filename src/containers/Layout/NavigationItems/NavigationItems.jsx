import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import List, {ListItem, ListItemIcon, ListItemText} from 'material-ui/List';
import Collapse from 'material-ui/transitions/Collapse';
import Business from 'material-ui-icons/Business';
import DashBoardIcon from 'material-ui-icons/Dashboard';
import ExpandLess from 'material-ui-icons/ExpandLess';
import ExpandMore from 'material-ui-icons/ExpandMore';
import {NavLink} from 'react-router-dom';
import slugify from 'slugify';

import * as Routes from '../../../routes/Routes';
import Palette from '../../../styles/Palette';
import classesNav from './NavigationItems.scss';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: Palette.primary,
        color: Palette.white
    },
    nested: {
        paddingLeft: theme.spacing.unit * 6,
        boxShadow: 'inset 0 -1px ' + Palette.primary
    },
    active: {
        boxShadow: 'inset 0 -1px ' + Palette.primary + ', inset 3px 0 ' + Palette.active
    },
    collapse: {
        background: Palette.submenu,
        padding: '0'
    },
    button: {
        /*padding: '12px 16px',*/
    }
});

class Navigationitems extends Component {
    state = {open: false};

    componentDidMount = () => {
        if (window.location.pathname.split("/")[1] === 'empresa') {
            this.setState({open: true});
        }
    };

    handleClick = () => {
        if(this.props.openedDrawer) {
            this.setState({open: !this.state.open});
        }
        this.props.onMenuClicked();
    };

    verifyActiveLink = (match, location) => {
        if (match) {
            return match.path.split("/")[1] === location.pathname.split("/")[1];
        }
    };

    render() {
        const {classes} = this.props;
        return (
            <div className={[classes.root, classesNav.NavigationItems].join(' ')}>
                <ListItem
                    button
                    component={NavLink}
                    to={Routes.HOME}
                    className={classes.button}
                    onClick={this.props.onItemClicked}
                >
                    <ListItemIcon>
                        <DashBoardIcon/>
                    </ListItemIcon>
                    <ListItemText primary="Home"/>
                </ListItem>

                <ListItem
                    button
                    onClick={this.handleClick}
                    className={classes.button}>
                    <ListItemIcon>
                        <Business/>
                    </ListItemIcon>
                    <ListItemText inset primary="Empresas"/>
                    {this.state.open && this.props.openedDrawer ? <ExpandLess/> : <ExpandMore/>}
                </ListItem>
                <Collapse in={this.state.open && this.props.openedDrawer} timeout="auto" unmountOnExit
                          classes={{container: classes.collapse}}>
                    <List component="div" disablePadding>
                        {this.props.items.map(item => (
                            <ListItem
                                key={item.id}
                                button
                                className={classes.nested}
                                component={NavLink}
                                to={{
                                    pathname: Routes.EMPRESA + `/${slugify(item.nombre, {lower: true})}`,
                                    state: {companySelected: item}
                                }}
                                isActive={this.verifyActiveLink}
                                activeClassName={classes.active}
                                onClick={this.props.onItemClicked}
                            >
                                <ListItemText primary={item.nombre}/>
                            </ListItem>
                        ))}
                    </List>
                </Collapse>
            </div>
        );
    }
}

Navigationitems.propTypes = {
    classes: PropTypes.object.isRequired,
    openedDrawer: PropTypes.bool.isRequired,
    items: PropTypes.array.isRequired,
    onItemClicked: PropTypes.func.isRequired,
    onMenuClicked: PropTypes.func.isRequired
};

export default withStyles(styles)(Navigationitems);