import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import classNames from 'classnames';
import Drawer from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Divider from 'material-ui/Divider';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui-icons/Menu';
import ChevronRigtIcon from 'material-ui-icons/ChevronRight';
import Close from 'material-ui-icons/Close';
import Fade from 'material-ui/transitions/Fade';
import List from 'material-ui/List';

import Dialog from '../../components/UI/Dialog/Dialog';
import Button from '../../components/UI/Button/Button';
import {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';

import * as authActions from '../../redux-modules/auth/actions';
import * as businessActions from '../../redux-modules/business/actions';

import Palette from '../../styles/Palette';

import NavigationItems from './NavigationItems/NavigationItems';
import User from './User/User';

import logoBlanco from '../../assets/img/logo_sisamex_modificado.png';
import logoBlancoMini from '../../assets/img/sisamex_logo_sin_texto.png';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
        height: '100%',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
    },
    appFrame: {
        height: '100%',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        paddingLeft: theme.spacing.unit * 8,
        transition: theme.transitions.create(['width', 'margin','padding'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarMobile: {
        paddingLeft: 0,
        transition: theme.transitions.create(['width', 'margin','padding'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        paddingLeft: 0,
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin', 'padding'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginLeft: 12,
        marginRight: 20,
    },
    hide: {
        display: 'none',
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        backgroundColor: Palette.primary,
        color: Palette.white,
        transition: theme.transitions.create(['width','left'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        [theme.breakpoints.down('md')]: {
            position: 'fixed',
            left: 0,
        },
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create(['width','left'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing.unit * 7,
        [theme.breakpoints.down('md')]: {
            left: -(theme.spacing.unit * 7),
        },
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '0 8px',
        ...theme.mixins.toolbar,
        color: Palette.white,
    },
    iconButtonHeader : {
        color: Palette.primary
    },
    divider: {
       margin: '0 25px',
       backgroundColor: Palette.white
    },
    content: {
        flexGrow: 1,
        backgroundColor: Palette.background,
        padding: `${theme.spacing.unit * 11}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
        [theme.breakpoints.down('md')]: {
            padding: '65px 14px 24px',
        }
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    logo: {
        //width: '70%'
        height: '30px'
    }
});

class Layout extends Component {
    state = {
        openedDrawer: false,
        openedDialog: false,
        anchor: 'left',
        isMobile: true,
    };

    componentDidMount = () => {
        this.props.onFetchBusinesIfNeeded();
        const isMobile = window.innerWidth < 480;
        if(!isMobile) {
            this.setState({openedDrawer: true, isMobile: false});
        }
    };

    handleDrawerOpen = () => {
        if(!this.state.openedDrawer) {
            this.setState({ openedDrawer: true });
        }
    };

    handleDrawerClose = () => {
        this.setState({ openedDrawer: false });
    };

    handleOpenLogoutDialog = () => {
        this.setState({openedDialog: true});
    };

    handleCloseLogoutDialog = () => {
        this.setState({openedDialog: false});
    };
    handleAcceptedLogoutDialog = () => {
        this.setState({openedDialog: false});
        this.props.onLogout();
    };

    render() {
        const { classes, theme } = this.props;
        const { anchor, openedDrawer } = this.state;

        let navigation = null;
        if(this.props.business.length > 0) {
            navigation = <NavigationItems
                items={this.props.business}
                onItemClicked={() => this.handleDrawerClose()}
                onMenuClicked={() => this.handleDrawerOpen()}
                openedDrawer={openedDrawer}
            />;
        }

        const drawer = (
            <Drawer
                variant="permanent"
                classes={{
                    paper: classNames(classes.drawerPaper, !this.state.openedDrawer && classes.drawerPaperClose),
                }}
                open={this.state.openedDrawer}
            >
                <div className={classes.drawerHeader}>
                    <img src={this.state.openedDrawer ? logoBlanco : logoBlancoMini} alt="SISAMEX" className={classes.logo}/>
                </div>
                <Divider classes={{root: classes.divider}}/>
                <List>{navigation}</List>
            </Drawer>
        );

        let userLogoutDialog = null;
        if(this.state.openedDialog) {
            userLogoutDialog = <Dialog
                    open={this.state.openedDialog}
                    onClose={this.handleCloseLogoutDialog}
                    aria-labelledby="dialog-title"
                >
                    <DialogTitle id="dialog-title">Cerrar sesión</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            ¿Desea cerrar su sesión?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseLogoutDialog} color="secondary">
                            Cancelar
                        </Button>
                        <Button onClick={this.handleAcceptedLogoutDialog} color="primary" autoFocus>
                            Aceptar
                        </Button>
                    </DialogActions>
                </Dialog>;
        }

        return (
            <Fade in timeout={500}>
                <div style={{width: '100%'}}>
                    <div className={classes.root}>
                        <div className={classes.appFrame}>
                            <AppBar
                                position="absolute"
                                className={classNames(
                                    this.state.isMobile ? classes.appBarMobile : classes.appBar,
                                    this.state.openedDrawer && classes.appBarShift
                                )}
                                color="inherit"
                            >
                                <Toolbar disableGutters={!this.state.open}>
                                    <IconButton
                                        className={classNames(classes.iconButtonHeader,!openedDrawer && classes.hide)}
                                        onClick={this.handleDrawerClose}>
                                        {theme.direction === 'rtl' ? <ChevronRigtIcon /> : <Close />}
                                    </IconButton>
                                    <IconButton
                                        color="inherit"
                                        aria-label="open drawer"
                                        onClick={this.handleDrawerOpen}
                                        className={classNames(classes.menuButton, openedDrawer && classes.hide)}
                                    >
                                        <MenuIcon />
                                    </IconButton>
                                    <Fade
                                        in={this.state.isMobile ? !openedDrawer : true}
                                        timeout={{ enter: 500, exit: 100,}}
                                    >
                                        <User
                                            name={this.props.user.fullName}
                                            email={this.props.user.email}
                                            logoutClicked={() => this.handleOpenLogoutDialog()}
                                        />
                                    </Fade>

                                </Toolbar>
                            </AppBar>
                            {drawer}
                            <main
                                className={classNames(classes.content, classes[`content-${anchor}`], {
                                    [classes.contentShift]: openedDrawer,
                                    [classes[`contentShift-${anchor}`]]: openedDrawer,
                                })}
                            >


                                {this.props.children}

                                {userLogoutDialog}
                            </main>
                        </div>
                    </div>
                </div>
            </Fade>
        );
    }
}

Layout.propTypes = {
  business: PropTypes.array.isRequired,
  children: PropTypes.any.isRequired,
  classes: PropTypes.object.isRequired,
  onFetchBusinesIfNeeded: PropTypes.func.isRequired,
  onLogout: PropTypes.func.isRequired,
  theme: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: {
            company: state.auth.user.company,
            company_id: state.auth.user.company_id,
            fullName: state.auth.user.fullName,
            name: state.auth.user.fullName,
            email: state.auth.user.email
        },
        business: state.business.business
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onLogout: bindActionCreators(authActions.logout, dispatch),
        onFetchBusinesIfNeeded: bindActionCreators(businessActions.fetchBusinessIfNeeded, dispatch),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(Layout));