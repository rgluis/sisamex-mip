import React, {Component} from 'react';
import ReactGA from 'react-ga';

import AppleLogo from '../../assets/img/apple.png';
import PlayStoreLogo from '../../assets/img/play-store.png';
import classes from './Home.scss';

class Home extends Component {
    componentDidMount = () => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    };

    handleClickApp = app => {
        ReactGA.event({
            category: 'APP',
            action: 'Click/Download',
            label: app
        })
    };

    render() {
        return(
            <div className={classes.Home}>
                <a
                    onClick={() => this.handleClickApp('Apple')}
                    className={classes.Link}
                    href="itms-services://?action=download-manifest&url=https://dl.dropboxusercontent.com/s/30hh34aiapqqq3q/manifest.plist"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <div className={classes.AppContainer}>
                        <div className={classes.AppLogoContainer}>
                            <div className={classes.AppLogo}>
                                <img src={AppleLogo} alt="Apple"/>
                            </div>
                        </div>

                        <div className={classes.AppTextContainer}>
                            <p>Disponible en</p>
                            <p>Apple Store</p>
                        </div>
                    </div>
                </a>

                <a
                    onClick={() => this.handleClickApp('Android')}
                    className={classes.Link}
                    href="https://dl.dropboxusercontent.com/s/kml4vfxki4o6fsn/monitoreo_medios.apk"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <div className={classes.AppContainer}>
                        <div className={classes.AppLogoContainer}>
                            <div className={classes.AppLogo}>
                                <img src={PlayStoreLogo} alt="Play Store"/>
                            </div>
                        </div>

                        <div className={classes.AppTextContainer}>
                            <p>Disponible en</p>
                            <p>Google Play</p>
                        </div>
                    </div>
                </a>
            </div>
        );
    }
}

export default Home;