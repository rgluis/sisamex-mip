import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Typography} from 'material-ui';
import { withStyles } from 'material-ui/styles';
import Select from 'material-ui/Select';
import { MenuItem } from 'material-ui/Menu';
import { InputLabel } from 'material-ui/Input';
import { FormControl } from 'material-ui/Form';
import Tooltip from 'material-ui/Tooltip';
import {
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
} from 'material-ui/Dialog';

import Button from '../../components/UI/Button/Button';
import Dialog from '../../components/UI/Dialog/Dialog';

import AddIcon from 'material-ui-icons/Add';

import CompanyContainer from './CompanyContainer/CompanyContainer';
import classesDashboard from './Dashboard.scss';
import * as userActions from '../../redux-modules/user/actions';

import Palette from  '../../styles/Palette';

const styles = theme => ({
    title: {
        color: Palette.titles,
        fontWeight: '300',
        marginBottom: '16px'
    },
    buttonStart: {
        margin: theme.spacing.unit,
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2,
    },
    buttonAdd: {
        margin: theme.spacing.unit,
        position: 'absolute',
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2,
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 200,
        maxWidth: 300,
    }
});


class Dashboard extends Component {
    state = {
        openedDialogAddCompany: false,
        openedDialogDeleteCompany: false,
        companyToDeleteId: null,
    };

    shouldComponentUpdate(nextProps, nextState) {
        return this.props.businessDashboard.length !== nextProps.businessDashboard.length || this.props.business !== nextProps.business.length;
    }

    handleConfigureDashboard = () => {
        this.setState({openedDialogAddCompany: true});
    };

    handleCloseDialog = () => {
        this.setState({openedDialogAddCompany: false});
    };

    handleChange = event => {
        const companySelected = this.props.business.find(company => company.id === event.target.value);
        this.props.onAddBusinessToDashboard(companySelected);
        this.handleCloseDialog();
    };

    handleDeleteCompanyFromDashboard = () => {
        this.props.onDeleteCompanyFromDashboard(this.state.companyToDeleteId);
        this.setState({openedDialogDeleteCompany: false, companyToDeleteId: null});
    };

    openDeleteDialog = (companyId) => {
        this.setState({openedDialogDeleteCompany: true, companyToDeleteId: companyId});
    };

    handleCloseDeleteCompanyDialog = () => {
        this.setState({openedDialogDeleteCompany: false, companyToDeleteId: null});
    };

    render() {
        console.log("Dashboard render");
        let dashboard = (
            <React.Fragment>
                <Typography
                    variant="display3"
                    classes={{root: this.props.classes.title}}
                >
                    Configura tu dashboard
                </Typography>
                <Button
                    className={this.props.classes.buttonStart}
                    variant="raised"
                    color="primary"
                    onClick={this.handleConfigureDashboard}
                >
                    Empezar
                </Button>
            </React.Fragment>
        );
        if(this.props.businessDashboard.length > 0) {
            dashboard = (
                <div className={classesDashboard.DashboardBusiness}>
                    {this.props.businessDashboard.map(company => (
                        <CompanyContainer
                            key={company.id}
                            company={company}
                            onDeleteClicked={(companyId) => this.openDeleteDialog(companyId)}
                        />
                    ))}

                    {this.props.businessDashboard.length !== this.props.business.length ?
                        <Tooltip id="tooltip-add-company" title="Agregar Empresa" placement="top-start">
                            <Button
                                variant="fab"
                                className={this.props.classes.buttonAdd}
                                color="primary"
                                onClick={this.handleConfigureDashboard}
                            >
                                <AddIcon />
                            </Button>
                        </Tooltip>
                        : null
                    }

                    <Dialog
                        open={this.state.openedDialogDeleteCompany}
                        onClose={this.handleCloseDeleteCompanyDialog}
                        aria-labelledby="delete-company"
                    >
                        <DialogTitle id="delete-company">Eliminar Empresa</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                ¿Está seguro de eliminar está empresa de su dashboard?
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleCloseDeleteCompanyDialog} color="secondary">
                                Cancelar
                            </Button>

                            <Button onClick={this.handleDeleteCompanyFromDashboard} color="primary">
                                Aceptar
                            </Button>
                        </DialogActions>
                    </Dialog>
                </div>
            );
        }
        return (
            <React.Fragment>
                <div className={classesDashboard.Dashboard}>
                    {dashboard}
                </div>


                <Dialog
                    open={this.state.openedDialogAddCompany}
                    onClose={this.handleCloseDialog}
                    aria-labelledby="add-company"
                >
                    <DialogTitle id="add-company">Agregar Empresa</DialogTitle>
                    <DialogContent>
                        <FormControl className={this.props.classes.formControl}>
                            <InputLabel htmlFor="company">Empresa</InputLabel>
                            <Select
                                value=""
                                onChange={this.handleChange}
                                inputProps={{
                                    name: 'company',
                                    id: 'company',
                                }}
                            >
                                {this.props.business ? this.props.business.map(company => (
                                    <MenuItem
                                        key={company.id}
                                        value={company.id}
                                    >
                                        {company.nombre}
                                    </MenuItem>
                                )) : null}
                            </Select>
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseDialog} color="secondary">
                            Cancelar
                        </Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        business: state.auth.user.business,
        businessDashboard: state.user.businessDashboard,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onAddBusinessToDashboard: bindActionCreators(userActions.addBusinessToDashboard, dispatch),
        onDeleteCompanyFromDashboard: bindActionCreators(userActions.deleteCompanyFromDashboard, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Dashboard));