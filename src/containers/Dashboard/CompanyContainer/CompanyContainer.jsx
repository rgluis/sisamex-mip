import React, {Component} from 'react';
import Paper from 'material-ui/Paper';
import { withStyles } from 'material-ui/styles';

import IconButton from 'material-ui/IconButton';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import DeleteIcon from 'material-ui-icons/Delete';

import classesCompanyContainer from './CompanyContainer.scss';
import Palette from "../../../styles/Palette";

const styles = theme => ({
    title: {
        color: Palette.titles,
        fontWeight: '300',
        marginBottom: '32px'
    },
    buttonStart: {
        margin: theme.spacing.unit,
        bottom: theme.spacing.unit * 2,
        right: theme.spacing.unit * 2,
    },
});

class CompanyContainer extends Component {

    render() {

        return (
            <div style={{width: '100%'}}>
                <header className={classesCompanyContainer.CompanyContainerHeader}>
                    <img src={this.props.company.logo} alt={this.props.company.nombre}/>

                    <div className={classesCompanyContainer.CompanyContainerHeaderActions}>
                        <IconButton
                            color="primary"
                            aria-label="Delete"
                            onClick={() => this.props.onDeleteClicked(this.props.company.id)}
                        >
                            <DeleteIcon />
                        </IconButton>
                    </div>
                </header>
                <Paper className={classesCompanyContainer.CompanyContainer}>
                    <Typography
                        variant="display2"
                        classes={{root: this.props.classes.title}}
                    >
                        Configurar Empresa
                    </Typography>
                    <Button
                        className={this.props.classes.buttonStart}
                        variant="raised"
                        color="primary"
                    >
                        Agregar máquinas
                    </Button>
                </Paper>
            </div>
        );
    }
}

export default withStyles(styles)(CompanyContainer);