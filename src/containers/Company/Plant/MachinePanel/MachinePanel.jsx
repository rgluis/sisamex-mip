import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { withStyles } from 'material-ui/styles';
import ExpansionPanel, {
    ExpansionPanelSummary,
    ExpansionPanelDetails,
} from 'material-ui/ExpansionPanel';
import Typography from 'material-ui/Typography';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';
import Grid from 'material-ui/Grid';
import {Fade} from 'material-ui/transitions';

import MeterIndicator from '../../../../components/MeterIndicator/MeterIndicator';
import TrafficLightIndicator from '../../../../components/TrafficLightIndicator/TrafficLightIndicator';
import VariableDetail from '../VariableDetail/VariableDetail';
import TrafficLight from '../../../../components/UI/Icons/TrafficLight/TrafficLight';
import * as machineActions from '../../../../redux-modules/machines/actions';
import arrToObj from '../../../../Utils/arrToObj';

const styles = theme => ({
    root: {
        flexGrow: 1,
        alignSelf: 'flex-start',
        width: '100%'
    },
    verticalCenter : {
        alignSelf: 'center'
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
        marginRight: '16px'
    },
    panelContentExpanded: {
        alignItems: 'center',
        '& > :last-child': {
            paddingRight: 0
        }
    },
    panelRootExpanded: {
        boxShadow: '0 3px 29px -18px',
    },
    panelRootDetails: {
        padding: '24px',
    }
});

class MachinePanel extends Component {

    state = {
        expanded: null,
        intervalId: null,
        filteredMachines: {},
        machinesWithDetail: {},
        machineDetailActive: false,
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        if(nextProps.searchValue !== this.props.searchValue ||
            Object.keys(nextState.filteredMachines).length !== Object.keys(this.state.filteredMachines).length ||
            nextState.expanded !== this.state.expanded) {
            return true;
        }

        if(Object.keys(nextState.machinesWithDetail).length !== Object.keys(this.state.machinesWithDetail).length) {
            return true;
        }

        if(nextState.machineDetailActive !== this.state.machineDetailActive) { return true; }

        if(nextState.expanded) {
            const machineSelectedVariables = this.props.machines[nextState.expanded].detalle_variables;
            const nextMachineSelectedVariables = nextProps.machines[nextState.expanded].detalle_variables;
            return JSON.stringify(machineSelectedVariables) !== JSON.stringify(nextMachineSelectedVariables);
        }

        return false;
    };

    componentDidUpdate = () => {
        if(this.props.searchValue || this.props.searchValue === "") {
            this.handleSearch(this.props.searchValue);
        }
    };

    componentDidMount = () => {
        //console.log("Machines: ", this.props.machines);
        this.startUpdateMachine();
    };

    componentWillUnmount = () => {
        clearInterval(this.state.intervalId);
    };

    updateMachine = (plantSelectedId, machineId) => {
        return setInterval(() => {
            this.props.onUpdateMachine(plantSelectedId, machineId);
        },3000);
    };

    startUpdateMachine = () => {
        clearInterval(this.state.intervalId);
        const machinesIds = Object.keys(this.props.machines);
        if(machinesIds.length > 0) {
            this.setState({
                expanded: this.props.machines[machinesIds[0]].id,
                intervalId: this.updateMachine(this.props.plantSelected, this.props.machines[machinesIds[0]].id)
            });
        }
    };

    handleSearch = (searchValue) => {
        const filteredMachines = Object.keys(this.props.machines).filter(machineId => {
            if(searchValue.trim() === "") {
                return false;
            }
            return this.props.machines[machineId].nombre.indexOf(searchValue.trim().toUpperCase()) > -1;
        }).map(machineId => {
            return this.props.machines[machineId];
        });

        const filteredMachinesObj = filteredMachines.length > 0 ? arrToObj(filteredMachines) : {};

        this.setState(prevState => {
            return {
                filteredMachines: filteredMachinesObj,
                expanded: filteredMachines.length === 1 ?
                    filteredMachines[0].id :
                    prevState.expanded
            };
        });
    };

    handleChange = panel => (event, expanded) => {
        clearInterval(this.state.intervalId);
        this.setState({
            expanded: expanded ? panel : false,
            intervalId: this.updateMachine(this.props.plantSelected, panel),
        });
    };

    handleMeterIndicatorClick = (machineId, variableId, variableName, variableUnit) => {

        clearInterval(this.state.intervalId);
        this.setState(prevState => {
            return {
                machineDetailActive: true,
                machinesWithDetail: {
                    ...prevState.machinesWithDetail,
                    [machineId]: {
                        machineId: parseInt(machineId, 10),
                        variableId: parseInt(variableId, 10),
                        variableName: variableName,
                        variableUnit: variableUnit,
                    }
                }
            }
        });
    };

    closeVariableDetail = () => {
        this.setState({
            machineDetailActive: false,
            intervalId: this.updateMachine(this.props.plantSelected, this.state.expanded),
        });
    };
    calculatePercentage = (value, umbral_maxima) => {
        const newPercentage = (parseFloat(value) / parseFloat(umbral_maxima)) * 100;

        return newPercentage > 100 ? 100 : parseInt(newPercentage, 10);
    };
    render() {
        const { classes, machines } = this.props;
        let machinesToShow = null;

        if(Object.keys(this.state.filteredMachines).length > 0) {
            machinesToShow = this.state.filteredMachines;
        }else if(Object.keys(this.state.filteredMachines).length === 0 && this.props.searchValue.length > 0) {
            machinesToShow = null;
        }else {
            machinesToShow = machines;
        }

        return (
            <div className={[classes.root, !machinesToShow ? classes.verticalCenter : null].join(" ")}>
                {machinesToShow ? Object.keys(machinesToShow).map(machineId => (
                    <ExpansionPanel
                        key={machinesToShow[machineId].id}
                        expanded={this.state.expanded === machinesToShow[machineId].id}
                        onChange={this.handleChange(machinesToShow[machineId].id)}>
                        <ExpansionPanelSummary classes={{root:classes.panelRootExpanded, content: classes.panelContentExpanded }} expandIcon={<ExpandMoreIcon />}>
                            <Typography className={classes.heading}>{machinesToShow[machineId].nombre}</Typography>
                            {machinesToShow[machineId].indicador_color === 'amarillo' || machinesToShow[machineId].indicador_color === 'rojo' ?
                                <TrafficLight color={machinesToShow[machineId].indicador_color} animated /> :
                                null
                            }
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails classes={{root: classes.panelRootDetails}}>
                            <Grid container justify="space-around" alignItems="center">
                                {
                                    this.state.machineDetailActive && this.state.machinesWithDetail[machineId]
                                        ?
                                        <Fade in>
                                            <VariableDetail
                                                machineId={this.state.machinesWithDetail[machineId].machineId}
                                                variableId={this.state.machinesWithDetail[machineId].variableId}
                                                variableName={this.state.machinesWithDetail[machineId].variableName}
                                                variableUnit={this.state.machinesWithDetail[machineId].variableUnit}
                                                token={this.props.token}
                                                onClickedClose={() => this.closeVariableDetail()}
                                            />
                                        </Fade>
                                        :
                                        machinesToShow[machineId].detalle_variables.map((variable,index) => (
                                            <Grid item xs={12} sm={6} md={6} lg={4} key={index}>
                                                {variable.umbral_maxima === 0 ?
                                                    <TrafficLightIndicator
                                                        realValue={variable.valor}
                                                        color={variable.indicador_color}
                                                        title={variable.variable}
                                                        unit={variable.unidad_medida}
                                                    />
                                                    :
                                                    <MeterIndicator
                                                        formatValue={this.calculatePercentage(variable.valor, variable.umbral_maxima)}
                                                        realValue={variable.valor}
                                                        color={variable.indicador_color}
                                                        title={variable.variable}
                                                        unit={variable.unidad_medida}
                                                        onClicked={() => this.handleMeterIndicatorClick(machineId, variable.variable_id, variable.variable, variable.unidad_medida)}
                                                    />
                                                }
                                            </Grid>
                                        ))
                                }
                            </Grid>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                )) :
                    <Typography variant="display1" color="primary" style={{textAlign: 'center'}}>No se han encontrado resultados.</Typography>}
            </div>
        );
    }
}

const mapStateToProps = state => {
    const {
        plantSelected,
    } = state.plants[state.business.companySelected];
    return {
        plantSelected,
        token: state.auth.token,
        machines: state.machines[plantSelected].machines
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onUpdateMachine: bindActionCreators(machineActions.updateMachine ,dispatch),
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(MachinePanel));

MachinePanel.propTypes = {
  classes: PropTypes.object.isRequired,
  machines: PropTypes.object.isRequired,
  plantSelected: PropTypes.number.isRequired,
  searchValue: PropTypes.string,
  token: PropTypes.string.isRequired
};