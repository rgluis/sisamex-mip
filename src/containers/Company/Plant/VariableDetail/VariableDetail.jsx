import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {SortingState, IntegratedSorting, GroupingState, IntegratedGrouping} from '@devexpress/dx-react-grid';
import {Grid, TableHeaderRow, VirtualTable, TableGroupRow} from '@devexpress/dx-react-grid-material-ui';
import {ResponsiveContainer, AreaChart, XAxis, YAxis, CartesianGrid, Tooltip, Area, Label} from 'recharts';

import axios from '../../../../axios-sisamex';

import {CircularProgress} from 'material-ui/Progress';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';
import Button from '../../../../components/UI/Button/Button';
import BackIcon from 'material-ui-icons/KeyboardArrowLeft';
import Cell from './Cell/Cell';

import Pallete from '../../../../styles/Palette';
import classes from './VariableDetail.scss';



class VariableDetail extends Component {
    state = {
        isFetching: true,
        error: false,
        dataForTable: {},
        dataForGraph: {},
        dataGroup: [],
        sorting: [{ columnName: 'hora', direction: 'asc' }],
        selection: [],
    };

    changeSorting = sorting => this.setState({ sorting });

    componentDidMount = () => {
        return axios({
            url: `/api/variable/?maquina_id=${this.props.machineId}&variable_id=${this.props.variableId}`,
            headers: {
                "Authorization": "JWT " + this.props.token
            }
        })
            .then(response => {
                if(response.data.length === 0 || response.data.length === 1) { throw new Error("Error en eServer"); }
                if(typeof(response.data) === 'string') { throw new Error("No se encuentra la variable"); }
                if(response.data.promedio.length === 0){ throw new Error("No hay datos que mostrar"); }
                const graphData = response.data.promedio.map(dt => {
                    return {
                        "hora": this.leadingZero(dt.hora),
                        "promedio": parseFloat(dt.promedio.toFixed(2))
                    };
                });

                let dataForTable = [];
                response.data.reporte
                    .map(reporte => {
                        return reporte.data.map(data => {
                            return dataForTable.push({
                                "horaGroup": `${data.hora}:00`,
                                "hora": `${data.hora}:${data.minutos}`,
                                "indicador_color": data.indicador_color,
                                "valor": data.valor + data.unidad_medida,
                            });
                        });
                    });

                const dataGroup = response.data.reporte.map(data => {
                    return data.titulo;
                });

                this.setState({
                    dataForGraph: graphData,
                    dataForTable: dataForTable,
                    dataGroup: dataGroup,
                    isFetching: false,
                });
            })
            .catch(error => {
                console.error(error);
                this.setState({
                    error: true,
                    isFetching: false,
                });
            });
    };

    leadingZero = hour => {
        return Array(
            +(2 - hour.toString().length + 1 > 0 && 2 - hour.toString().length + 1)
        ).join("0") + hour;
    };

    changeSelection = selection => this.setState({ selection });

    render() {
        return (
            <div className={classes.VariableDetail} style={this.props.style}>
                {this.state.isFetching && !this.state.error ?
                    <CircularProgress
                        size={50}
                        color="primary"
                    /> :
                    this.state.error ?
                        <React.Fragment>
                            <Typography
                                color="primary"
                                style={{textAlign: 'center', display: 'block', width: '100%',marginBottom: '2rem'}}
                            >
                                En esté momento no podemos consultar está información.
                            </Typography>
                            <Button
                                variant="raised"
                                color="primary"
                                onClick={this.props.onClickedClose}
                            >
                                Atrás
                            </Button>
                        </React.Fragment>:
                        <React.Fragment>
                            <header className={classes.HeaderTitleContainer}>
                                <Button
                                    variant="fab"
                                    mini
                                    color="primary"
                                    onClick={this.props.onClickedClose}
                                    className={classes.BackIcon}
                                >
                                    <BackIcon />
                                </Button>
                                <Typography
                                    variant="display1"
                                    color="primary"
                                    className={classes.HeaderTitle}
                                >
                                    Detalle de variable <span style={{textTransform: 'capitalize'}}>{this.props.variableName}</span>
                                </Typography>
                            </header>
                            <div className={classes.GraphContainer}>
                                <Paper className={classes.PaperContainer}>
                                    <Typography
                                        color="primary"
                                        variant="headline"
                                        gutterBottom
                                        align="center"
                                    >
                                        Promedio Hora x Hora
                                    </Typography>
                                    <ResponsiveContainer>
                                        <AreaChart width={500} height={300} data={this.state.dataForGraph}
                                                   margin={{ top: 10, right: 30, left: 0, bottom: 15 }}
                                        >
                                            <defs>
                                                <linearGradient id="promedio" x1="0" y1="0" x2="0" y2="1">
                                                    <stop offset="5%" stopColor={Pallete.primary} stopOpacity={0.8}/>
                                                    <stop offset="95%" stopColor={Pallete.primary} stopOpacity={0.8}/>
                                                </linearGradient>
                                            </defs>
                                            <XAxis dataKey="hora">
                                                <Label value="Horas" className={classes.GraphLabel} offset={-10} position="insideBottom" />
                                            </XAxis>
                                            <YAxis
                                                dataKey="promedio"
                                                domain={['auto', 'auto']}
                                            >
                                                <Label value={this.props.variableUnit} angle={-90} className={classes.GraphLabel} offset={10} position="insideLeft" />
                                            </YAxis>
                                            <CartesianGrid strokeDasharray="3 3" />
                                            <Tooltip />
                                            <Area dot={{ fill: Pallete.background, strokeWidth: 1 }} type="monotone" dataKey="promedio" stroke={Pallete.primary} fillOpacity={1} fill="url(#promedio)" />
                                        </AreaChart>
                                    </ResponsiveContainer>
                                </Paper>
                            </div>
                            <div className={classes.TableDetail}>
                                <Paper className={classes.PaperContainer}>
                                    <Typography
                                        color="primary"
                                        variant="headline"
                                        gutterBottom
                                        align="center"
                                    >
                                        Reporte Críticos
                                    </Typography>
                                    <Grid
                                        rows={this.state.dataForTable}
                                        columns={[
                                            {name: 'horaGroup', title: 'Hora'},
                                            {name: 'hora', title: 'Hora'},
                                            {name: 'indicador_color', title: 'Estatus'},
                                            {name: 'valor', title: 'Valor'},
                                        ]}
                                    >
                                        <SortingState
                                            sorting={this.state.sorting}
                                            onSortingChange={this.changeSorting}
                                        />
                                        <GroupingState
                                            grouping={[{ columnName: 'horaGroup' }]}
                                            expandedGroups={this.state.dataGroup}
                                        />
                                        {/* <SelectionState
                                        selection={this.state.selection}
                                        onSelectionChange={this.changeSelection}
                                    />
                                    <PagingState
                                        defaultCurrentPage={0}
                                        pageSize={6}
                                    />
                                    <IntegratedSelection />
                                    <IntegratedPaging />
                                    <Table />
                                    <TableHeaderRow showSortingControls />
                                    <TableSelection showSelectAll />
                                    <PagingPanel />*/}
                                        <IntegratedGrouping />
                                        <IntegratedSorting />
                                        <VirtualTable
                                            height={290}
                                            cellComponent={Cell}
                                            messages={{noData: 'Aún no hay información crítica del día'}}
                                        />
                                        <TableHeaderRow showSortingControls />
                                        <TableGroupRow />
                                    </Grid>
                                </Paper>
                            </div>
                        </React.Fragment>
                }
            </div>
        );
    }
}

export default VariableDetail;

VariableDetail.propTypes = {
  machineId: PropTypes.number.isRequired,
  onClickedClose: PropTypes.func.isRequired,
  style: PropTypes.object,
  token: PropTypes.string.isRequired,
  variableId: PropTypes.number.isRequired,
  variableName: PropTypes.string.isRequired,
  variableUnit: PropTypes.string.isRequired
};