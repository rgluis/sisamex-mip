import React from 'react';
import { TableCell } from 'material-ui/Table';
import TrafficLight from '../../../../../components/UI/Icons/TrafficLight/TrafficLight';

const cell = (props) => {
    return (
        <TableCell>
            {props.column.name === 'indicador_color' ?
                <TrafficLight color={props.value}/> :
                props.value
            }
        </TableCell>
    );
};

export default cell;