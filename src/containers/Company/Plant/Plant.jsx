import React, {Component} from 'react';
import ReactGA from 'react-ga';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {InputAdornment} from 'material-ui/Input';
import TextField from 'material-ui/TextField';
import SearchIcon from 'material-ui-icons/Search';

import Typography from 'material-ui/Typography';
import {CircularProgress} from 'material-ui/Progress';

import MachinePanel from './MachinePanel/MachinePanel';

import classes from './Plant.scss';
import * as machineActions from "../../../redux-modules/machines/actions";

class Plant extends Component {
    state={
        searchValues: {},
    };

    componentDidMount = () => {
        ReactGA.pageview(window.location.pathname + window.location.search);
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        if(nextState.searchValues[nextProps.plantSelected]) {
            if(this.state.searchValues[nextProps.plantSelected]) {
                if(Object.keys(nextState.searchValues[nextProps.plantSelected]).length !== Object.keys(this.state.searchValues[nextProps.plantSelected]).length) {
                    return true;
                }
            }else {
                return true;
            }
        }else if(nextState.searchValues[nextProps.plantSelected] !== this.state.searchValues[nextProps.plantSelected]) {
            return true;
        }

        return nextProps.isFetching !== this.props.isFetching;
    };

    /*componentWillUpdate = () => {
        console.log("update plant", this.props.plantSelectedFull.indicador_color);
    };*/

    handleSearch = (event) => {
        this.setState({
            searchValues: {
                ...this.state.searchValues,
                [this.props.plantSelected]: event.target.value
            }
        });
    };

    render() {
        let machines = <CircularProgress
            className={classes.progress}
            size={50}
            color="primary"
        />;

        if(!this.props.isFetching) {
            machines = <MachinePanel
                searchValue={this.state.searchValues[this.props.plantSelected] ? this.state.searchValues[this.props.plantSelected] : ''}
            />
        }
        return (
            <div className={classes.Plant}>
                <header className={classes.PlantHeader}>
                    <Typography variant="display2" color="primary">{this.props.plantSelectedFull.nombre}</Typography>
                    <TextField
                        id="search-machine"
                        onChange={this.handleSearch}
                        value={
                            this.state.searchValues[this.props.plantSelected] ?
                                this.state.searchValues[this.props.plantSelected] :
                                ''
                        }
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <SearchIcon />
                                </InputAdornment>
                            ),
                        }}
                    />
                </header>
                <section className={classes.PlantMachines}>
                    {machines}
                </section>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const {
        plantSelected,
    } = state.plants[state.business.companySelected];
    return {
        plantSelectedFull: state.plants[state.business.companySelected].plants[plantSelected],
        plantSelected,
        isFetching: state.machines[plantSelected].isFetching,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        updateMachinesStatuses: bindActionCreators(machineActions.updateMachinesStatuses, dispatch),
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(Plant);

Plant.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  plantSelected: PropTypes.number.isRequired,
  plantSelectedFull: PropTypes.object.isRequired
};