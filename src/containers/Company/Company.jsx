import React, {Component} from 'react';
import ReactGA from 'react-ga';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import { withRouter, Redirect, Route } from 'react-router-dom';

import { withStyles } from 'material-ui/styles';
import Drawer from 'material-ui/Drawer';
import ListSubheader from 'material-ui/List/ListSubheader';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';
import AppBar from 'material-ui/AppBar'
import Toolbar from 'material-ui/Toolbar';
import Tabs, { Tab } from 'material-ui/Tabs';
import slugify from 'slugify';

import TrafficLight from '../../components/UI/Icons/TrafficLight/TrafficLight';
import Palette from '../../styles/Palette';
import * as Routes from '../../routes/Routes';
import * as businessActions from '../../redux-modules/business/actions';
import * as plantsActions from '../../redux-modules/plants/actions';
import Plant from './Plant/Plant';
import * as machinesActions from "../../redux-modules/machines/actions";

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
        height: '100%',
        width: '100%',
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        backgroundColor: Palette.white,
        boxShadow: '0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 5px 0px rgba(0, 0, 0, 0.12)',
    },
    drawerPaper: {
        position: 'relative',
        width: drawerWidth,
        overflow: 'visible',
    },
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: '88px 24px 24px',
        width: '100%',
        minWidth: 0,
        [theme.breakpoints.down('md')]: {
            padding: '10px 0 58px 0',
            background: 'none',
        }
    },
    toolbar: theme.mixins.toolbar,
    headerToolbar: {
        justifyContent: 'center',
    },
    headerLogo: {
        maxHeight: '40px',
        maxWidth: '200px'
    },
    listItemIcon: {
        marginRight: '0',
    },
    listSubHeader: {
        textAlign: 'center'
    },
    listSubHeaderTitle: {
        boxShadow: '0 1px ' + Palette.primary,
        padding: '0 10px',
        color: Palette.primary,
        fontSize: '0.9rem'
    },
    listItemActive: {
        boxShadow: '0 2px 4px -2px ' + Palette.primary,
        position: 'relative',
        cursor: 'pointer',
        '&:after': {
            position: 'absolute',
            content: '""',
            width: '34px',
            height: '34px',
            right: '-16px',
            background: Palette.white,
            boxShadow: '0 2px 3px -2px ' + Palette.primary + ', 2px 0 2px -3px ' + Palette.primary,
            transform: 'rotate(-45deg)'
        },
        '&:hover': {
            textDecoration: 'none'
        }
    },
    listItem: {
        boxShadow: '0 1px 2px -2px ' + Palette.primary,
        cursor: 'pointer',
        '&:hover': {
            textDecoration: 'none'
        }
    },
    tabRoot: {
        height: '58px',
        textTransform: 'none'
    },
    tabLabelContainer: {
        paddingTop: '10px',
        paddingBottom: 0
    },
    tabsButtons: {
        flex: '0 0 40px',
    },
    appBarTabs: {
        top: 'unset',
        bottom: 0
    }
});

class Company extends Component {
    constructor(props) {
        super(props);

        this.companySlug = this.props.match.params.companySlug;

        this.companySelected = this.props.business.find(company => {
            return slugify(company.nombre, {lower: true}) === this.companySlug;
        });
    }

    state = {
        computedClickedPlant: false,
        intervalId: null,
        isMobile: false,
        activeTab: 0,
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        if(this.state.computedClickedPlant !== nextState.computedClickedPlant || nextProps.location.pathname !== this.props.location.pathname) {
            return true;
        }

        const verifyColorsChange = Object.keys(nextProps.plants).filter(plant => {
            return nextProps.plants[plant].indicador_color !== this.props.plants[plant].indicador_color;
        });

        return verifyColorsChange.length > 0;
    };

    componentWillMount = () => {
        const isMobile = window.innerWidth < 480;
        if(isMobile) {
            this.setState({isMobile: true});
        }
        if (this.companySelected) {
            ReactGA.event({
                category: 'BUSINESS',
                action: 'select',
                label: this.companySlug
            });
            this.props.selectCompany(this.companySelected.id);
            this.props.onFetchPlantsIfNeeded(this.companySelected.id);
        }
    };

    componentWillReceiveProps = (nextProps, prevState)  => {
        const nextPlantsIds = Object.keys(nextProps.plants);
        if(((nextPlantsIds.length > 0 && !this.state.computedClickedPlant) || (nextPlantsIds.length > 0 && nextProps.match.isExact)) && nextProps.companySelectedId) {
            ReactGA.pageview(window.location.pathname + window.location.search);
            this.handlePlantClick(nextProps.plants[nextPlantsIds[0]].id, slugify(nextProps.plants[nextPlantsIds[0]].nombre, {lower: true}), nextProps.companySelectedId);
            this.setState({computedClickedPlant: true});
        }
    };

    componentDidMount = () => {
        if(this.companySelected) {
            const updatePlants = setInterval(() => {
                this.props.updatePlantsIndicators(this.companySelected.id);
            }, 3000);
            this.setState({intervalId: updatePlants});
        }
    };

    componentWillUnmount = () => {
        clearInterval(this.state.intervalId);
        this.props.selectCompany(null);
    };

    handlePlantClick = (plantId, plantSlug, companyId) => {
        this.props.selectPlant(plantId, this.props.companySelectedId);
        this.props.onFetchMachines(this.props.token, plantId, companyId);
        this.props.history.push(`${this.props.match.url}/${plantSlug}`);
    };

    handleChangeTabs = (event, value) => {
        this.setState({ activeTab: value });
    };

    render() {
        const { classes, plants } = this.props;
        if(!this.companySelected) {
            return <Redirect to={Routes.HOME}/>;
        }

        let plantsNavigation = null;
        if(plants) {
            plantsNavigation = Object.keys(plants).map(key => {
                if(this.state.isMobile) {
                    return <Tab
                        key={plants[key].id}
                        onClick={() => this.handlePlantClick(plants[key].id,slugify(plants[key].nombre, {lower:true}), this.props.companySelectedId)}
                        label={plants[key].nombre}
                        icon={<TrafficLight color={plants[key].indicador_color} animated />}
                        classes={{
                            root: classes.tabRoot,
                            labelContainer: classes.tabLabelContainer
                        }}
                    />;
                }else {
                    return (<ListItem
                        onClick={() => this.handlePlantClick(plants[key].id,slugify(plants[key].nombre, {lower:true}), this.props.companySelectedId)}
                        className={plants[key].id === this.props.plantSelected ? classes.listItemActive : classes.listItem}
                        key={plants[key].id}
                    >
                        <ListItemIcon className={classes.listItemIcon}>
                            <TrafficLight color={plants[key].indicador_color} animated />
                        </ListItemIcon>
                        <ListItemText inset primary={plants[key].nombre} />
                    </ListItem>)
                }
            });
        }

        return (
            <div className={classes.root}>
                {this.state.isMobile ?
                    <AppBar
                        position="fixed"
                        className={classes.appBar}
                        classes={{root: classes.appBarTabs}}
                    >
                        <Tabs
                            value={this.state.activeTab}
                            onChange={this.handleChangeTabs}
                            /*scrollable
                            scrollButtons="on"*/
                            fullWidth
                            indicatorColor="primary"
                            textColor="primary"
                            classes={{
                                scrollButtons: classes.tabsButtons,
                            }}
                        >
                            {plantsNavigation}
                        </Tabs>
                    </AppBar> :
                    <React.Fragment>
                        <AppBar position="absolute" className={classes.appBar}>
                            <Toolbar className={classes.headerToolbar}>
                                <img src={this.companySelected.logo} alt={this.companySelected.nombre} className={classes.headerLogo}/>
                            </Toolbar>
                        </AppBar>
                        <Drawer
                            variant="permanent"
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                        >
                            <div className={classes.toolbar} />
                            <List
                                component="nav"
                                subheader={<ListSubheader component="div" className={classes.listSubHeader}><span className={classes.listSubHeaderTitle}>Plantas</span></ListSubheader>}
                            >
                                {plantsNavigation}
                            </List>
                        </Drawer>
                    </React.Fragment>
                }
                <main className={classes.content}>
                    <Route path={this.props.match.url + '/:plantSlug'} component={Plant}/>
                </main>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const companySelectedId = state.business.companySelected;
    const {
        isFetching,
        plants,
        plantSelected,
    } = state.plants[companySelectedId] || {
        isFetching: true,
        plantSelected: null,
        plants: {}
    };

    return {
        business: state.business.business,
        token: state.auth.token,
        isFetching,
        plantSelected,
        plants,
        companySelectedId
    };
};

const mapDispatchToProps = dispatch => {
    return {
        selectCompany: bindActionCreators(businessActions.selectCompany, dispatch),
        onFetchPlantsIfNeeded: bindActionCreators(plantsActions.fetchPlantsIfNeeded, dispatch),
        selectPlant: bindActionCreators(plantsActions.selectPlant, dispatch),
        onFetchMachines: bindActionCreators(machinesActions.fetchMachines, dispatch),
        updatePlantsIndicators: bindActionCreators(plantsActions.updatePlantsIndicators, dispatch),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Company)));

Company.propTypes = {
  business: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
  companySelectedId: PropTypes.number,
  onFetchMachines: PropTypes.func.isRequired,
  onFetchPlantsIfNeeded: PropTypes.func.isRequired,
  plantSelected: PropTypes.number,
  plants: PropTypes.object.isRequired,
  selectCompany: PropTypes.func.isRequired,
  selectPlant: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
  updatePlantsIndicators: PropTypes.func.isRequired
};