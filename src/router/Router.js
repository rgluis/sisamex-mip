import React, {Component} from 'react'
import {connect} from 'react-redux';
import {
    withRouter,
    Route,
    Redirect,
    Switch
} from 'react-router-dom'

import * as routesDeclarations from '../routes/Routes';

import Auth from "../containers/Auth/Auth";
import Home from "../containers/Home/Home";
import Dashboard from '../containers/Dashboard/Dashboard';
import Layout from '../containers/Layout/Layout';
import Company from '../containers/Company/Company';

class PrivateRoute extends Component {

    render() {
        const { component: Component, ...rest } = this.props;
        return (
            <Route
                {...rest}
                render={props => {
                    return this.props.isAuthenticated ? (
                        <Layout>
                            <Component {...props} />
                        </Layout>
                    ) : (
                        <Redirect
                            to={{
                                pathname: routesDeclarations.LOGIN,
                            }}
                        />
                    )
                }}
            />
        )
    }
}
const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null,
    };
};

PrivateRoute = withRouter(connect(mapStateToProps)(PrivateRoute));

const Routes = () => (
    <React.Fragment>
        <Switch>
            <Route path={routesDeclarations.LOGIN} exact component={Auth} />
            {/*<Layout>
                <PrivateRoute path={routesDeclarations.DASHBOARD} exact component={Dashboard} />
                <PrivateRoute path={routesDeclarations.EMPRESA + '/:companySlug'} component={Company} />
            </Layout>*/}

            <PrivateRoute path={routesDeclarations.HOME} exact component={Home} />
            <PrivateRoute path={routesDeclarations.DASHBOARD} exact component={Dashboard} />
            <PrivateRoute path={routesDeclarations.EMPRESA + '/:companySlug'} component={Company} />
            <Redirect to={routesDeclarations.LOGIN}/>
        </Switch>
    </React.Fragment>
);

export default Routes;