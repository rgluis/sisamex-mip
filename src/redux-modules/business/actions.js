import axios from '../../axios-sisamex';
import * as types from './types';
import logo from '../../assets/img/logo_sisamex.png';

const fetchBusinessSuccess = (business) => {
    const businessWithLogo = business.map(company => ({
        ...company,
        logo: logo,
    }));
    return {
        type: types.FETCH_BUSINESS_SUCCESS,
        business: businessWithLogo
    };
};

const fetchBusinessStart = () => {
    return {
        type: types.FETCH_BUSINESS_START
    };
};

const fetchBusinessFail = (error) => {
    return {
        type: types.FETCH_BUSINESS_FAIL,
        errorMessage: error
    };
};

export const fetchBusiness = token => {
    return dispatch => {
        dispatch(fetchBusinessStart());
        axios({
            url: "/api/empresas/",
            headers: {
                "Authorization": "JWT " + token
            }
        })
            .then(response => {
                dispatch(fetchBusinessSuccess(response.data));
            })
            .catch(error => {
                // TODO: Acción en caso de que servidor no responda en empresas
                console.log(error);
                dispatch(fetchBusinessFail(error));
            });
    };
};

function shouldFetchBusiness(state) {
    const business = state.business;
    if (business.business.length === 0) {
        return true;
    } else if (business.isFetching) {
        return false;
    }
}

export function fetchBusinessIfNeeded() {
    return (dispatch, getState) => {
        if (shouldFetchBusiness(getState())) {
            return dispatch(fetchBusiness(getState().auth.token))
        }
    }
}

export function selectCompany(companyId) {
    return {
        type: types.SELECT_COMPANY,
        companyId: companyId
    };
}