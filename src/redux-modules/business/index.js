import * as types from './types';

const initialState = {
    business: [],
    isFetching: false,
    companySelected: null,
    fetchError: false,
    errorMessage: null
};

const fetchBusinessStart = (state, action) => {
    return {
        ...state,
        isFetching: true,
    };
};

const fetchBusinessFail = (state, action) => {
    return {
        ...state,
        isFetching: false,
        fetchError: true,
        errorMessage: action.error
    };
};

const fetchBusinessSuccess = (state, action) => {
    return {
        ...state,
        business: action.business,
        isFetching: false
    };
};

const selectCompany = (state, action) => {
    return {
        ...state,
        companySelected: action.companyId
    };
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_BUSINESS_START: return fetchBusinessStart(state,action);
        case types.FETCH_BUSINESS_SUCCESS: return fetchBusinessSuccess(state,action);
        case types.FETCH_BUSINESS_FAIL: return fetchBusinessFail(state,action);
        case types.SELECT_COMPANY: return selectCompany(state, action);
        default:
            return state;
    }
};

export default reducer;