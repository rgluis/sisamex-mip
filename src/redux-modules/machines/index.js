import * as types from './types';
import arrToObj from '../../Utils/arrToObj';

const initialMachinesState = {
    isFetching: false,
    machines: [],
    fetchError: false,
    updateError: false,
    errorMessage: null
};

const fetchMachinesStart = (state,action) => {
    return {
        ...state,
        isFetching: true,
    };
};

const fetchMachinesSuccess = (state, action) => {
    const machines = arrToObj(action.machines);
    return {
        ...state,
        isFetching: false,
        machines: machines,
    };
};

const fetchMachinesFail = (state, action) => {
    return {
        ...state,
        fetchError: true,
        errorMessage: action.errorMessage
    };
};

const updateMachinesStatusesSuccess = (state, action) => {
    const machines = arrToObj(action.machines);
    return {
        ...state,
        machines: machines
    };
};

const updateMachinesStatusesFail = (state, action) => {
    return {
        ...state,
        updateError: true,
        errorMessage: action.error
    };
};

const updateMachineStart = (state,action) => {
    return {
        ...state,
        isUpdating: true,
    };
};

const updateMachineFail = (state,action) => {
    return {
        ...state,
        isUpdating: false,
        updateError: false,
    };
};

const updateMachineSuccess = (state, action) => {
    const machines = arrToObj(action.machines);
    return {
        ...state,
        isUpdating: false,
        detalle_variables: machines[action.machineId].detalle_variables
    };
};

/*const fetchVariableDetailSuccess = (state, action) => {
    const variableKey = state.detalle_variables.findIndex(variable => {
        return variable.variable === action.variableName;
    });
    if(variableKey) {
        return {
            ...state,
            detalle_variables: [
                ...state.detalle_variables,
                state.detalle_variables[variableKey] = {
                    ...state.detalle_variables[variableKey],
                    isFetching: false,
                    detail: {
                        ...action.recordData
                    }
                }
            ]
        };
    }
    return state;
};

const fetchVariableDetailStart = (state, action) => {
    const variableKey = state.detalle_variables.findIndex(variable => {
        return variable.variable === action.variableName;
    });
    if(variableKey) {
        return {
            ...state,
            detalle_variables: [
                ...state.detalle_variables,
                state.detalle_variables[variableKey] = {
                    ...state.detalle_variables[variableKey],
                    isFetching: true
                }
            ]
        };
    }
    return state;
};*/

function machine(state, action) {
    switch (action.type) {
        case types.UPDATE_MACHINE_START: return updateMachineStart(state, action);
        case types.UPDATE_MACHINE_FAIL: return updateMachineFail(state, action);
        case types.UPDATE_MACHINE_SUCCESS: return updateMachineSuccess(state, action);
        /*case types.FETCH_VARIABLE_DETAIL_SUCCESS: return fetchVariableDetailSuccess(state, action);
        case types.FETCH_VARIABLE_DETAIL_START: return fetchVariableDetailStart(state, action);*/
        default:
            return state;
    }
}

function machines(state = initialMachinesState, action) {
    switch (action.type) {
        case types.FETCH_MACHINES_START: return fetchMachinesStart(state, action);
        case types.FETCH_MACHINES_SUCCESS: return fetchMachinesSuccess(state, action);
        case types.FETCH_MACHINES_FAIL: return fetchMachinesFail(state, action);
        case types.UPDATE_MACHINES_STATUSES_SUCCESS: return updateMachinesStatusesSuccess(state, action);
        case types.UPDATE_MACHINES_STATUSES_FAIL: return updateMachinesStatusesFail(state, action);
        case types.UPDATE_MACHINE_START:
        case types.UPDATE_MACHINE_FAIL:
        case types.UPDATE_MACHINE_SUCCESS:
       /* case types.FETCH_VARIABLE_DETAIL_START:
        case types.FETCH_VARIABLE_DETAIL_SUCCESS:*/
            return {
                ...state,
                machines: {
                    ...state.machines,
                    [action.machineId]: machine(state.machines[action.machineId], action)
                }
            };
        default:
            return state;
    }
}

const reducer = (state = {}, action) => {
    switch(action.type) {
        case types.FETCH_MACHINES_START:
        case types.FETCH_MACHINES_SUCCESS:
        case types.FETCH_MACHINES_FAIL:
        case types.UPDATE_MACHINE_START:
        case types.UPDATE_MACHINE_FAIL:
        case types.UPDATE_MACHINE_SUCCESS:
        case types.UPDATE_MACHINES_STATUSES_FAIL:
        case types.UPDATE_MACHINES_STATUSES_SUCCESS:
        /*case types.FETCH_VARIABLE_DETAIL_START:
        case types.FETCH_VARIABLE_DETAIL_SUCCESS:*/
            return {
                ...state,
                [action.plantId]: machines(state[action.plantId], action)
            };
        default:
            return state;
    }
};

export default reducer;