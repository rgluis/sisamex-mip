import axios from '../../axios-sisamex';
import * as types from './types';

const fetchMachinesStart = plantId => {
    return {
        type: types.FETCH_MACHINES_START,
        plantId: plantId
    };
};

const fetchMachinesSuccess = (machines, plantId) => {
    const machinesStatuses = machines.map(machine => {
        if (plantId.indicador_color === 'verde') {
            return {
                ...machine,
                indicador_color: 'verde'
            };
        } else {
            let amarillo = 0;
            let rojo = 0;

            machine.detalle_variables.forEach(variable => {
                if (variable.indicador_color === 'amarillo') {
                    amarillo++
                }
                if (variable.indicador_color === 'rojo') {
                    rojo++
                }
            });

            return {
                ...machine,
                indicador_color: rojo > 0 ? 'rojo' : amarillo > 0 ? 'amarillo' : 'verde'
            };
        }
    });

    return {
        type: types.FETCH_MACHINES_SUCCESS,
        plantId: plantId,
        machines: machinesStatuses
    };
};

const fetchMachinesFail = (error, plantId) => {
    return {
        type: types.FETCH_MACHINES_FAIL,
        errorMessage: error,
        plantId: plantId
    };
};

const updateMachineStart = (plantId, machineId) => {
    return {
        type: types.UPDATE_MACHINE_START,
        plantId: plantId,
        machineId: machineId
    };
};

const updateMachineFail = (error, plantId, machineId) => {
    return {
        type: types.UPDATE_MACHINE_FAIL,
        errorMessage: error,
        plantId: plantId,
        machineId: machineId
    };
};

const updateMachinesStatusesFail = (error, plantId) => {
    return {
        types: types.UPDATE_MACHINES_STATUSES_FAIL,
        errorMessage: error,
        plantId: plantId
    };
};

const updateMachinesStatusesSuccess = (machines, plantId) => {
    const machinesStatuses = machines.map(machine => {
        if (plantId.indicador_color === 'verde') {
            return {
                ...machine,
                indicador_color: 'verde'
            };
        } else {
            let amarillo = 0;
            let rojo = 0;

            machine.detalle_variables.forEach(variable => {
                if (variable.indicador_color === 'amarillo') {
                    amarillo++
                }
                if (variable.indicador_color === 'rojo') {
                    rojo++
                }
            });

            return {
                ...machine,
                indicador_color: rojo > 0 ? 'rojo' : amarillo > 0 ? 'amarillo' : 'verde'
            };
        }
    });
    return {
        types: types.UPDATE_MACHINES_STATUSES_SUCCESS,
        plantId: plantId,
        machines: machinesStatuses
    };
};

const updateMachineSuccess = (machines, plantId, machineId) => {
    return {
        type: types.UPDATE_MACHINE_SUCCESS,
        plantId: plantId,
        machineId: machineId,
        machines: machines
    };
};

export const fetchMachines = (token, plantId, companyId) => {
    return dispatch => {
        dispatch(fetchMachinesStart(plantId));
        axios({
            url: `/api/maquinas/?empresa_id=${companyId}&planta_id=${plantId}`,
            headers: {
                "Authorization": "JWT " + token
            }
        })
            .then(response => {
                if(response.data.length === 0) { throw new Error("Error en eServer"); }
                dispatch(fetchMachinesSuccess(response.data, plantId));
            })
            .catch(error => {
                console.error(error);
                dispatch(fetchMachinesFail(error, plantId));
            });
    };
};

function shouldFetchMachines(state, plantSelectedId) {
    const machines = state.machines[plantSelectedId];
    if(!machines) {
        return true;
    } else if(machines.isFetching) {
        return false;
    }
}

export function fetchMachinesIfNeeded(plantSelectedId) {
    return (dispatch, getState) => {
        if (shouldFetchMachines(getState(), plantSelectedId)) {
            return dispatch(fetchMachines(getState().auth.token, plantSelectedId, getState().business.companySelected))
        }
    }
}

export const updateMachine = (plantId, machineId) => {
    return (dispatch, getState) => {
        dispatch(updateMachineStart(plantId, machineId));
        axios({
            url: `/api/maquinas/?empresa_id=${getState().business.companySelected}&planta_id=${plantId}`,
            headers: {
                "Authorization": "JWT " + getState().auth.token
            }
        })
            .then(response => {
                if(response.data.length === 0) { throw new Error("Error en eServer"); }
                dispatch(updateMachineSuccess(response.data, plantId, machineId));
            })
            .catch(error => {
                console.error(error);
                dispatch(updateMachineFail(error, plantId, machineId));
            });
    };
};

export const updateMachinesStatuses = (plantId) => {
    return (dispatch, getState) => {
        axios({
            url: `/api/maquinas/?empresa_id=${getState().business.companySelected}&planta_id=${plantId}`,
            headers: {
                "Authorization": "JWT " + getState().auth.token
            }
        })
            .then(response => {
                if(response.data.length === 0) { throw new Error("Error en eServer"); }
                dispatch(updateMachinesStatusesSuccess(response.data, plantId));
            })
            .catch(error => {
                console.error(error);
                dispatch(updateMachinesStatusesFail(error, plantId));
            });
    };
};

/*
const fetchVariableDetailStart = (plantId, machineId, variableName) => {
    return {
        type: types.FETCH_VARIABLE_DETAIL_START,
        plantId: plantId,
        machineId: machineId,
        variableName: variableName
    };
};

const fetchVariableDetailSuccess = (plantId, machineId, variableName, recordData) => {
    return {
        type: types.FETCH_VARIABLE_DETAIL_SUCCESS,
        plantId: plantId,
        machineId: machineId,
        variableName: variableName,
        recordData: recordData
    };
};

export const fetchVariableDetail = (plantId, machineId, variableName) => {
    return dispatch => {
        dispatch(fetchVariableDetailStart(plantId, machineId, variableName));

        const recordData = [
            {name: '00:00', temperature: '38°'},
            {name: '01:00', temperature: '40°'},
            {name: '02:00', temperature: '45°'},
            {name: '03:00', temperature: '25°'},
            {name: '04:00', temperature: '30°'},
            {name: '05:00', temperature: '28°'},
            {name: '06:00', temperature: '31°'},
            {name: '07:00', temperature: '24°'},
            {name: '08:00', temperature: '30°'},
            {name: '09:00', temperature: '35°'},
            {name: '10:00', temperature: '38°'},
            {name: '11:00', temperature: '41°'},
        ];

        setTimeout(() => {
            dispatch(fetchVariableDetailSuccess(plantId, machineId, variableName, recordData))
        }, 1000);
    };
};*/
