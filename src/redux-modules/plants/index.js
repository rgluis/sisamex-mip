import * as types from './types';
import arrToObj from '../../Utils/arrToObj';

const initialPlantsState = {
    isFetching: false,
    //isUpdating: false,
    plantSelected: null,
    plants: {},
    fetchError: false,
    updateError: false,
    errorMessage: null
};

const fetchPlantsStart = (state,action) => {
    return {
        ...state,
        isFetching: true,
    };
};

const fetchPlantsSuccess = (state, action) => {
    const plantsArr = action.plants.map(plant => {
        delete plant.detalle_variables;
        return plant;
    });
    const plants = arrToObj(plantsArr);
    return {
        ...state,
        isFetching: false,
        plants: {
            ...state.plants,
            ...plants
        }
    };
};

const fetchPlantsFail = (state, action) => {
    return {
        ...state,
        fetchError: true,
        errorMessage: action.errorMessage
    };
};

const selectPlant = (state, action) => {
    return {
        ...state,
        plantSelected: action.plantId
    };
};

const updatePlantsStart = (state, action) => {
    return {
        ...state,
        //isUpdating: true
    }
};
const updatePlantsFail = (state, action) => {
    return {
        ...state,
        //isUpdating: false,
        updateError: true,
        errorMessage: action.errorMessage
    };
};

const updatePlantsSuccess = (state, action) => {
    const plants = arrToObj(action.plants);
    return {
        ...state,
        //isUpdating: false,
        plants: {
            ...state.plants,
            ...plants,
        }
    };
};

function plants(state = initialPlantsState, action) {
    switch (action.type) {
        case types.FETCH_PLANTS_START: return fetchPlantsStart(state, action);
        case types.FETCH_PLANTS_SUCCESS: return fetchPlantsSuccess(state, action);
        case types.FETCH_PLANTS_FAIL: return fetchPlantsFail(state, action);
        case types.SELECT_PLANT: return selectPlant(state, action);
        case types.UPDATE_PLANTS_START: return updatePlantsStart(state, action);
        case types.UPDATE_PLANTS_FAIL: return updatePlantsFail(state, action);
        case types.UPDATE_PLANTS_SUCCESS: return updatePlantsSuccess(state, action);
        default:
            return state;
    }
}

const reducer = (state = {}, action) => {
    switch(action.type) {
        case types.FETCH_PLANTS_START:
        case types.FETCH_PLANTS_FAIL:
        case types.FETCH_PLANTS_SUCCESS:
        case types.SELECT_PLANT:
        case types.UPDATE_PLANTS_START:
        case types.UPDATE_PLANTS_FAIL:
        case types.UPDATE_PLANTS_SUCCESS:
            return {
                ...state,
                [action.companyId]: plants(state[action.companyId], action)
            };
        default:
            return state;
    }
};

export default reducer;