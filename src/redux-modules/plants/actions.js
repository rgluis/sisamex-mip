import axios from '../../axios-sisamex';
import * as types from './types';

const fetchPlantsStart = companyId => {
    return {
        type: types.FETCH_PLANTS_START,
        companyId: companyId
    };
};

const fetchPlantsSuccess = (plants, companyId) => {
    return {
        type: types.FETCH_PLANTS_SUCCESS,
        companyId: companyId,
        plants: plants
    };
};

const fetchPlantsFail = (error, companyId) => {
    return {
        type: types.FETCH_PLANTS_FAIL,
        errorMessage: error,
        companyId: companyId
    };
};

const updatePlantsStart = (companyId) => {
    return {
        type: types.UPDATE_PLANTS_START,
        companyId: companyId
    }
};

const updatePlantsFail = (error, companyId) => {
    return {
        type: types.UPDATE_PLANTS_FAIL,
        companyId: companyId,
        errorMessage: error,
    }
};

const updatePlantsSuccess = (plants, companyId) => {
    return {
        type: types.UPDATE_PLANTS_SUCCESS,
        companyId: companyId,
        plants: plants
    };
};

export const fetchPlants = (token, companyId) => {
    return dispatch => {
        dispatch(fetchPlantsStart(companyId));
        axios({
            url: "/api/plantas/?empresa_id=" + companyId,
            headers: {
                "Authorization": "JWT " + token
            }
        })
            .then(response => {
                if(response.data.length === 0) { throw new Error("Error en eServer"); }
                dispatch(fetchPlantsSuccess(response.data, companyId));
            })
            .catch(error => {
                console.error(error);
                dispatch(fetchPlantsFail(error, companyId));
            });
    };
};

function shouldFetchPlants(state, companySelectedId) {
    const plants = state.plants[companySelectedId];
    if(!plants) {
        return true;
    } else if(plants.isFetching) {
        return false;
    }
}

export function fetchPlantsIfNeeded(companySelectedId) {
    return (dispatch, getState) => {
        if (shouldFetchPlants(getState(), companySelectedId)) {
            return dispatch(fetchPlants(getState().auth.token, companySelectedId))
        }
    }
}

export const selectPlant = (plantId, companyId) => {
    return {
        type: types.SELECT_PLANT,
        plantId: plantId,
        companyId: companyId
    }
};

export const updatePlantsIndicators = (companySelectedId) => {
    return (dispatch, getState) => {
        dispatch(updatePlantsStart(companySelectedId));
        axios({
            url: "/api/plantas/?empresa_id=" + companySelectedId,
            headers: {
                "Authorization": "JWT " + getState().auth.token
            }
        })
            .then(response => {
                if(response.data.length === 0) { throw new Error("Error en eServer"); }
                dispatch(updatePlantsSuccess(response.data, companySelectedId));
            })
            .catch(error => {
                console.error(error);
                dispatch(updatePlantsFail(error, companySelectedId));
            });
    };
};