//import axios from '../../axios-sisamex';
import * as types from './types';

export const addBusinessToDashboard = company => {
    return {
        type: types.ADD_BUSINESS_TO_DASHBOARD,
        company: company
    };
};

export const deleteCompanyFromDashboard = companyId => {
    return {
        type: types.DELETE_COMPANY_FROM_DASHBOARD,
        companyId: companyId
    };
};