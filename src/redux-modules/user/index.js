import * as types from './types';

const initialState = {
    businessDashboard: [],
    error: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.ADD_BUSINESS_TO_DASHBOARD:
            return {
                ...state,
                businessDashboard: [
                    ...state.businessDashboard,
                    action.company
                ]
            };
        case types.DELETE_COMPANY_FROM_DASHBOARD:
            const newBusinessDashboard = state.businessDashboard.filter(company => company.id !== action.companyId);
            return {
                ...state,
                businessDashboard: newBusinessDashboard
            };
        default:
            return state;
    }
};

export default reducer;