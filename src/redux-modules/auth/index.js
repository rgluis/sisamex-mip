import * as types from './types';

const initialState = {
    user: {
        business: [],
        company: null,
        company_id: null,
        active: null,
        fullName: null,
        id: null,
        email: null,
    },
    token: null,
    error: null,
    loading: false,
};

const authStart = (state, action) => {
    return {
        ...state,
        error: null,
        loading: true
    };
};

const authFail = (state, action) => {
    return {
        ...state,
        error: action.error,
        loading: false
    };
};

const authSuccess = (state, action) => {
    return {
        ...state,
        user: {
            ...state.user,
            company: action.empresa,
            company_id: action.empresa_id,
            active: action.activo,
            fullName: action.nombre_completo,
            id: action.usuario_id,
            email: action.email,
        },
        token: action.token,
        error: null,
        loading: false
    };
};

const authLogout = (state, action) => {
    return {
        ...state,
        user: {
            ...state.user,
            business: [],
            company: null,
            company_id: null,
            active: null,
            fullName: null,
            id: null,
            email: null,
        },
        token: null,
        error: null,
        loading: false
    };
};



const reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.AUTH_START: return authStart(state,action);
        case types.AUTH_SUCCESS: return authSuccess(state, action);
        case types.AUTH_FAIL: return authFail(state, action);
        case types.AUTH_LOGOUT: return authLogout(state,action);
        default:
            return state;
    }
};

export default reducer;