import { SubmissionError } from 'redux-form';
import ReactGA from 'react-ga';

import axios from '../../axios-sisamex';
import * as types from './types';

export const authStart = () => {
    return {
        type: types.AUTH_START
    };
};

export const authFail = (error) => {
    ReactGA.event({
        category: 'Auth',
        action: 'login-fail',
        label: 'Log In Fail'
    });
    return {
        type: types.AUTH_FAIL,
        error: error
    };
};

export const authSuccess = (data) => {
    ReactGA.event({
        category: 'Auth',
        action: 'login-success',
        label: 'Log In Success'
    });
    return {
        type:types.AUTH_SUCCESS,
        token: data.token,
        empresa: data.empresa,
        empresa_id: data.empresa_id,
        activo: data.activo,
        nombre_completo: data.nombre_completo,
        usuario_id: data.usuario_id,
        email: data.email
    };
};

export const logout = () => {
    ReactGA.event({
        category: 'Auth',
        action: 'logout',
        label: 'Log Out'
    });
    return {
        type: types.AUTH_LOGOUT
    };
};

export const auth = (user, password) => {
    return dispatch => {
        dispatch(authStart());

        const trimUser = user.trim();
        const email = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(trimUser) ? trimUser : trimUser + '@sisamex.com.mx';

        return axios.get('/api/login?email=' + email.toLowerCase() + '&password=' + password)
            .then(response => {
                if(response.data.non_field_errors) {
                    throw new Error(response.data.non_field_errors);
                }

                dispatch(authSuccess(response.data));
            })
            .catch(error => {
                dispatch(authFail(`${error}`));
                throw new SubmissionError({
                    _error: `${error}`
                });
            });
    };
};