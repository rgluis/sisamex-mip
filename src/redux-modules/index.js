import {combineReducers} from "redux";
import { reducer as formReducer } from 'redux-form'
import authReducer from "./auth";
import userReducer from "./user";
import businessReducer from "./business";
import plantsReducer from "./plants";
import machinesReducer from "./machines";

const rootReducer = combineReducers({
    auth: authReducer,
    business: businessReducer,
    plants: plantsReducer,
    machines: machinesReducer,
    user: userReducer,
    form: formReducer,
});

export default rootReducer;