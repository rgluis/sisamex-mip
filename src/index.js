import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import {store, persistor} from './store';

import './index.scss';
import Palette from './styles/Palette';
import App from './App';
import registerServiceWorker from './registerServiceWorker';


const theme = createMuiTheme({
    palette: {
        primary: {main: Palette.primary},
    },
    overrides: {
        MuiInput: {
            underline: {
                '&:before' : {
                    backgroundColor: Palette.primary,
                },
                '&:hover:not($disabled):before' : {
                    backgroundColor: Palette.primary,
                }
            }
        },
        MuiSelect: {
            root: {
                '&:before' : {
                    backgroundColor: Palette.primary,
                },
            }
        }
    },
});

const app = (
    <PersistGate persistor={persistor}>
        <Provider store={store}>
            <BrowserRouter>
                <MuiThemeProvider theme={theme}>
                    <App />
                </MuiThemeProvider>
            </BrowserRouter>
        </Provider>
    </PersistGate>

);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
