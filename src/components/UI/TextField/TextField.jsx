import React from 'react';
import {TextField} from 'material-ui';

const textField = (props) => {
    return (
        <TextField
            {...props}
        />
    );
};

export default textField;