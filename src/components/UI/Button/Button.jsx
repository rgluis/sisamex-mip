import React from 'react';
import {Button} from 'material-ui';

const styles = {
    textTransform: 'none',
    fontWeight: '400',
    disabled: {
        cursor: 'not-allowed'
    }
};

const button = (props) => {
    return (
        <Button style={styles} {...props}>{props.children}</Button>
    );
};

export default button;