import React from 'react';

import classes from './TrafficLight.scss';

const trafficLight = props => {
    return (
        <div style={props.style} className={[classes.TrafficLight, classes[props.color], props.animated ? classes.animated: null].join(" ")}>
            {props.children ? props.children : null}
        </div>
    );
};

export default trafficLight;