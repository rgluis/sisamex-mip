import React from 'react';
import Dialog from 'material-ui/Dialog';

const dialog = (props) => (
    <Dialog {...props}>
        {props.children}
    </Dialog>
);

export default dialog;