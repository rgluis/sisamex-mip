import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import OpenIcon from 'material-ui-icons/OpenInNew';

import classes from './MeterIndicator.scss';

class MeterIndicator extends PureComponent {
    state = {
        wasUpdated: false
    };

    componentWillReceiveProps = (nextProps, nextState) => {
        if((nextProps.color !== this.props.color) ||
            (nextProps.realValue !== this.props.realValue && nextProps.formatValue !== this.props.formatValue)) {
            this.setState({wasUpdated: true});
        }else {
            this.setState({wasUpdated: false});
        }
    };

    render() {
        const { color, formatValue, realValue, title, unit } = this.props;
        return (
            <div className={classes.meterContainer} onClick={this.props.onClicked}>
                <OpenIcon className={classes.meterIcon} />
                <div className={`circular-progress progress-${color}-${formatValue}`}>
                </div>
                <div
                    className={
                        [
                            classes.meterInfo,
                            classes[color]
                        ].join(" ")}
                >
                    <span>{realValue} {unit}</span>
                    <span className={title.length > 12 ? classes['smaller']: ''}>{title}</span>
                </div>
            </div>
        );
    }
};

export default MeterIndicator;

MeterIndicator.propTypes = {
  color: PropTypes.string.isRequired,
  formatValue: PropTypes.number.isRequired,
  onClicked: PropTypes.func.isRequired,
  realValue: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  unit: PropTypes.string
};