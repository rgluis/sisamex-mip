import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import TrafficLight from '../UI/Icons/TrafficLight/TrafficLight';

import classes from './TrafficLightIndicator.scss';

class TrafficLightIndicator extends PureComponent {
    state = {
        wasUpdated: false,
        timeOut: null
    };
    componentWillReceiveProps = (nextProps, nextState) => {
        if ((nextProps.color !== this.props.color) ||
            (nextProps.realValue !== this.props.realValue)) {
            this.setState({
                wasUpdated: true,
                timeOut: setTimeout(() => {
                    this.setState({
                        wasUpdated: false,
                        timeOut: null,
                    });
                }, 500)
            });
        }
    };

    componentWillUnmount = () => {
        clearTimeout(this.state.timeOut);
    };
    
    render() {
        const {color, realValue, unit, title} = this.props;
        return (
            <div className={classes.trafficLightIndicatorContainer}>
                <TrafficLight color={color} animated={color === 'amarillo' || color === 'rojo'} />
                <div
                    className={
                        [
                            classes.trafficLightInfo,
                            classes[color]
                        ].join(" ")
                    }
                >
                    <span className={this.state.wasUpdated ? classes.twinkle: null}>{realValue}{unit}</span>
                    <span className={title.length > 15 ? classes['smaller']: ''}>{title}</span>
                </div>
            </div>
        );
    }
};

export default TrafficLightIndicator;

TrafficLightIndicator.propTypes = {
  color: PropTypes.string.isRequired,
  realValue: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  unit: PropTypes.string
};