import axios from 'axios';

let BASE_URL = 'http://172.16.100.221:3000';
/*if (process.env.NODE_ENV === 'development') {
    BASE_URL = 'http://172.16.100.221:3000';
}*/

const instance = axios.create({
    baseURL: BASE_URL,
});

export default instance;