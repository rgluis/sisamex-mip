import React, {Component} from 'react';
import ReactGA from 'react-ga';
//import {connect} from 'react-redux';

import Router from './router/Router';

//import Layout from './containers/Layout/Layout';

class App extends Component {
    componentDidMount = () => {
        ReactGA.initialize('UA-118150704-1');
        ReactGA.pageview(window.location.pathname + window.location.search);
    };

    render() {
        return <Router />;
    }
}

export default App;
