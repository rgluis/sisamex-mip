const palette = {
    primary: '#2e3e4d',
    active: '#2bacf9',
    white: '#ffffff',
    background: '#eff3f6',
    titles: '#7f8fa3',
    submenu: '#1e2631',
};

export default palette;