export const breakpoints = {
    s  : '0',
    m  : '640px',
    l  : '1024px',
    xl : '1440px'
};